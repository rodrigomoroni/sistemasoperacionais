#include <stdio.h>
#include <stdlib.h>
#include "pingpong.h"
#include "datatypes.h"

int task_count; //contador das tarefas (referencia para o id)

task_t TaskMain, *TaskEx; //taskEx -> tarefa em execução/corrente...

// Inicializa o sistema operacional; deve ser chamada no inicio do main()
void pingpong_init()
{
    setvbuf(stdout, 0, _IONBF, 0); //padrão
    TaskMain.id = 0; //id do main por padrão: 0
    task_count = 1; //contador é iniciado em 1
    TaskEx = &TaskMain; //tarefa em execução é a main
}

// Cria uma nova tarefa. Retorna um ID> 0 ou erro.
int task_create(task_t *task, void (*start_func)(void *), void *arg)
{
    if (task != NULL) //verificação básica
    {
        getcontext(&(task->task_context)); //salva o contexto

        //valores do contexto (padrão)
        /********************************************/
        char *stack;
        stack = malloc(STACKSIZE);
        if (stack)
        {
            task->task_context.uc_stack.ss_sp = stack ;
            task->task_context.uc_stack.ss_size = STACKSIZE;
            task->task_context.uc_stack.ss_flags = 0;
            task->task_context.uc_link = 0;
        }
        else
        {
            perror ("Erro na criação da pilha: ");
            exit (1);
        }
        /*********************************************/

        task->id = task_count; //atribui o id para a tarefa
        task_count++; //incrementa o contador
        makecontext(&(task->task_context), (void *)start_func, 1, arg); //adiciona a função no contexto da tarefa
        return task->id; //retorna o id da tarefa
    }
    return -1; //retorna negativo em caso de erro
}

// Termina a tarefa corrente, indicando um valor de status encerramento
void task_exit(int exitCode)
{
    task_switch(&TaskMain); //transfere o processador para o Main
}

// alterna a execução para a tarefa indicada
int task_switch(task_t *task)
{
    if (task != NULL)
    {
        task_t *TaskAux = TaskEx; //salvo a tarefa em execução em um auxiliar
        TaskEx = task; //muda a tarefa em execução para *task
        swapcontext(&TaskAux->task_context, &task->task_context); //faz a troca de contexto, salva Aux e restaura task
        return 0; //retorna 0 -> sucesso
    }
    return -1; //retorna -1 -> fracasso
}

// retorna o identificador da tarefa corrente (main eh 0)
int task_id ()
{
    return TaskEx->id; //retorna o id da tarefa em execução (corrente)
}
