#include <stdio.h>
#include <stdlib.h>
#include "pingpong.h"
#include "queue.h"

struct sigaction action;
struct itimerval timer;

int task_count; //contador das tarefas (referencia para o id)
task_t TaskMain, Dispatcher, *TaskEx, *ready, *suspend; //TaskEx -> tarefa em execuçã e as filas

//a cada disparo do timer (tick), trata...
void tratador_ticks()
{
	if (TaskEx->type == 'u') //verifica se é uma tarefa de usuário
	{
		TaskEx->ticks--; //decrementa os ticks
		if (TaskEx->ticks == 0) //se o tick chegar em zero
		{
			task_yield(); //devolve o processador para o dispatcher
		}
	}
}

//set da prioridade dinamica (igual ao estática)
void task_setpriodin(task_t *task, int prio)
{
    if (prio >= -20 && prio <= 20)
        task->prio_din = prio;
}

// define a prioridade estática de uma tarefa (ou a tarefa atual)
void task_setprio (task_t *task, int prio)
{
    if (task == NULL)
        task = TaskEx;
    if (prio >= -20 && prio <= 20)
    {
        task->prio = prio;
        //como a prioridade estática é definida apenas uma vez
        //inicializo valor da prioridade dinamica (din) aqui
        task->prio_din = prio;
    }
}

// retorna a prioridade estática de uma tarefa (ou a tarefa atual)
int task_getprio (task_t *task)
{
    if (task == NULL)
        task = TaskEx;
    return task->prio;
}

// suspende uma tarefa, retirando-a de sua fila atual, adicionando-a à fila
// queue e mudando seu estado para "suspensa"; usa a tarefa atual se task==NULL
void task_suspend (task_t *task, task_t **queue)
{
    if (task == NULL)
        task = TaskEx;
    task->status = 's';
    queue_append((queue_t **) queue,(queue_t *) task);
    if (queue != NULL)
        queue_remove((queue_t **) &ready, (queue_t *) task);
}

// acorda uma tarefa, retirando-a de sua fila atual, adicionando-a à fila de
// tarefas prontas ("ready queue") e mudando seu estado para "pronta"
void task_resume (task_t *task)
{
    queue_remove((queue_t **) &suspend, (queue_t *) task);
    task->status = 'r';
    queue_append((queue_t **) &ready, (queue_t *) task);
}

//coloca a tarefa no final da fila e devolve processador para o despachante
void task_yield()
{
    if (TaskEx != &TaskMain) //evita entrada da main na fila
    {
        queue_remove((queue_t **) &ready, (queue_t *) TaskEx);
        queue_append((queue_t **) &ready, (queue_t *) TaskEx);
    }
    task_switch(&Dispatcher);
}

//escalonador FCFS, pega o primeiro na fila de prontas
task_t* scheduler()
{
	task_t *menor = ready;
    return menor; //tarefa com maior prioridade)
}

//função do despachante
void dispatcher()
{
    task_t *next;
    while (ready > 0)
    {
        next = scheduler();
        if (next)
        {
            task_switch(next);
        }
    }
    task_exit(0);
}

// Inicializa o sistema operacional; deve ser chamada no inicio do main()
void pingpong_init()
{
    setvbuf(stdout, 0, _IONBF, 0); //padrão
    TaskMain.id = 0;
	TaskMain.type = 'u';
    TaskEx = &TaskMain;
    task_count = 1;
	Dispatcher.type = 's';

	/********************************************/
	action.sa_handler = tratador_ticks; //função que será chamada a cada disparo
  	sigemptyset (&action.sa_mask);
  	action.sa_flags = 0 ;
  	if (sigaction (SIGALRM, &action, 0) < 0)
  	{
    	perror ("Erro em sigaction: ") ;
    	exit (1) ;
  	}
  	timer.it_value.tv_usec = 100;	// primeiro disparo, em micro-segundos
  	timer.it_interval.tv_usec = 1000;	// disparos subsequentes, em micro-segundos
  	if (setitimer (ITIMER_REAL, &timer, 0) < 0)
  	{
    	perror ("Erro em setitimer: ") ;
    	exit (1) ;
  	}
	/********************************************/

    task_create(&Dispatcher, (void *)dispatcher, NULL);
}

// Cria uma nova tarefa. Retorna um ID> 0 ou erro.
int task_create(task_t *task, void (*start_func)(void *), void *arg)
{
    if (task != NULL)
    {
        getcontext(&(task->task_context)); //inicializa o contexto
        /********************************************/
        char *stack;
        stack = malloc(STACKSIZE);
        if (stack)
        {
            task->task_context.uc_stack.ss_sp = stack ;
            task->task_context.uc_stack.ss_size = STACKSIZE;
            task->task_context.uc_stack.ss_flags = 0;
            task->task_context.uc_link = 0;
        }
        else
        {
            perror ("Erro na criação da pilha: ");
            exit (1);
        }
        /*********************************************/
        task->id = task_count; //atribui o id para a tarefa
        task_count++; //incrementa o contador
        makecontext(&(task->task_context), (void *)start_func, 1, arg); //adiciona a função no contexto
        if (task_count > 2) //evita que o Dispatcher entre na fila de prontas, veja que o if está após o incremento
		{
	        task_t *aux;
        	aux = task;
            queue_append((queue_t **) &ready,(queue_t *) aux); //adiciona tarefa na fila de prontas
			task->type = 'u';
		}
        return task->id; //retorna o id da tarefa
    }
    return -1; //retorna negativo em caso de erro
}
// Termina a tarefa corrente, indicando um valor de status encerramento
void task_exit(int exitCode)
{
	if (task_id() > 1) //se a tarefa não for o dispatcher
	{
		queue_remove((queue_t **) &ready, (queue_t *) TaskEx); //remove tarefa da fila de prontas
		task_switch(&Dispatcher); //devolve processador para o Despachante
	}
	else //se for dispatcher
	{
		task_switch(&TaskMain); //devolve o processador para o Main
	}    
}

// alterna a execução para a tarefa indicada
int task_switch(task_t *task)
{
    if (task != NULL)
    {
        task_t *TaskAux = TaskEx;
        TaskEx = task;
		TaskEx->ticks = 20; //tarefa recebe 20 ticks
        swapcontext(&TaskAux->task_context, &task->task_context);
        return 0;
    }
    return -1;
}

// retorna o identificador da tarefa corrente (main eh 0)
int task_id ()
{
    return TaskEx->id;
}
