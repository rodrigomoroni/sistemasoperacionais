# README #

PingPongOS

### What is this repository for? ###

Auxiliar na implementação do SO

### How do I get set up? ###

Mini Sistema Operacional para ajudar no aprendizado na disciplina de Sistemas Operacionais - S73
Curso: Sistemas de Informação - Departamento Acadêmico de Informática - Prof Marco Aurélio
Universidade Tecnológica Federal do Paraná - Campus Curitiba - Sede Centro
Aluno: Rodrigo Giácomo Moroni de Souza - 1795481

### Contribution guidelines ###

Material pertence ao Prof Maziero (UFPR/UTFPR)

### Who do I talk to? ###

rodrigomoroni@hotmail.com
