#include <stdio.h>
#include <stdlib.h>
#include "pingpong.h"
#include "queue.h"

//#define PRIO

struct sigaction action;
struct itimerval timer;

unsigned int ticks; //contador de ticks desde a inicialização do sistema
int task_count; //contador das tarefas (referencia para o id)
task_t TaskMain, Dispatcher, *TaskEx, *ready, *suspend, *sleep; //TaskEx -> tarefa em execução e as filas

//função que busca na fila de 'adormecidas' tarefas que já podem acordar
void wakeup()
{
	//se existe algúem na fila
	if (queue_size ((queue_t *) sleep) > 0)
	{
		task_t *first = sleep;
        if (first->wakeup >= systime()) //verifica para o primeiro elemento
		{
			queue_remove((queue_t **) &sleep, (queue_t *) first);
			first->status = 'r';
			queue_append((queue_t **) &ready, (queue_t *) first);
		}
		task_t *aux = first->next;
		while (aux != first) //percorre a fila
		{
			if (aux->wakeup >= systime()) //se o momento é igual ou já passou, acorda
			{
				queue_remove((queue_t **) &sleep, (queue_t *) aux);
				aux->status = 'r';
				queue_append((queue_t **) &ready, (queue_t *) aux);
			}
			aux = aux->next; //anda
		}
	}
}

// a tarefa corrente aguarda o encerramento de outra task
int task_join (task_t *task)
{
	if (task != NULL)
	{
		queue_remove((queue_t **) &ready, (queue_t *) TaskEx); //remove tarefa da fila de prontas
		TaskEx->status = 's';
		queue_append((queue_t **) &suspend, (queue_t *) TaskEx); //add na fila de suspensas
		TaskEx->depend = task;
		task_switch(&Dispatcher); //devolve processador para dispatcher	
		return task->exitCode;
	}
	return -1;
}
//retorna os ticks
unsigned int systime()
{
	return ticks;
}

//a cada disparo do timer (tick), trata...
void tratador_ticks()
{
	wakeup(); //ainda não econtrei a posição ideal
	ticks++;
	TaskEx->processor_use++;
	if (TaskEx->type == 'u') //verifica se é uma tarefa de usuário
	{
		TaskEx->ticks--; //decrementa os ticks
		if (TaskEx->ticks == 0) //se o tick chegar em zero
		{
			task_yield(); //devolve o processador para o dispatcher
		}
	}
}

//set da prioridade dinamica (igual ao estática)
void task_setpriodin(task_t *task, int prio)
{
    if (prio >= -20 && prio <= 20)
        task->prio_din = prio;
}

// define a prioridade estática de uma tarefa (ou a tarefa atual)
void task_setprio (task_t *task, int prio)
{
    if (task == NULL)
        task = TaskEx;
    if (prio >= -20 && prio <= 20)
    {
        task->prio = prio;
        //como a prioridade estática é definida apenas uma vez
        //inicializo valor da prioridade dinamica (din) aqui
        task->prio_din = prio;
    }
}

// retorna a prioridade estática de uma tarefa (ou a tarefa atual)
int task_getprio (task_t *task)
{
    if (task == NULL)
        task = TaskEx;
    return task->prio;
}
// suspende a tarefa corrente por t segundos
void task_sleep (int t)
{
	TaskEx->wakeup = 1000*t + systime(); //momento em que a tarefa irá acordar, t segundos após o momento atual
	TaskEx->status = 's'; //mudei status
	queue_append((queue_t **) &sleep, (queue_t *) TaskEx); //adicionei na fila de 'adormecidas'
	queue_remove((queue_t **) &ready,(queue_t *) TaskEx); //removi da fila de prontas
}
// suspende uma tarefa, retirando-a de sua fila atual, adicionando-a à fila
// queue e mudando seu estado para "suspensa"; usa a tarefa atual se task==NULL
void task_suspend (task_t *task, task_t **queue)
{
    if (task == NULL)
        task = TaskEx;
    task->status = 's';
    queue_append((queue_t **) queue,(queue_t *) task);
    if (queue != NULL)
        queue_remove((queue_t **) &ready, (queue_t *) task);
}

// acorda uma tarefa, retirando-a de sua fila atual, adicionando-a à fila de
// tarefas prontas ("ready queue") e mudando seu estado para "pronta"
void task_resume (task_t *task)
{
    queue_remove((queue_t **) &suspend, (queue_t *) task);
    task->status = 'r';
    queue_append((queue_t **) &ready, (queue_t *) task);
}

//coloca a tarefa no final da fila e devolve processador para o despachante
void task_yield()
{
	//agora todas tarefas (incluindo a Main são movidas para o final da fila)
    queue_remove((queue_t **) &ready, (queue_t *) TaskEx);
    queue_append((queue_t **) &ready, (queue_t *) TaskEx);
    task_switch(&Dispatcher);
}

/**Defino se vai utilizar escalonador de prioridades ou FCFS**/
#ifdef PRIO
task_t* scheduler() //escalonador por prioridade
{
	task_t *first = ready; //salvo a primeira tarefa da fila
    task_t *aux = ready; //aux para caminhar na lista
    task_t *menor = aux; //menor é o aux (apenas inicial) -> mais prioritaria

    aux = aux->next; //anda na lista para entrar no laço
    while (aux != first)
    {
        if (aux->prio_din <= menor->prio_din) //se for menor, aux é o novo menor
            menor = aux;
        aux = aux->next;
    }

    //ajusto a prioridade dinamica das demais tarefas -> envelhecimento ou task aging
    aux = ready;
    if (aux != menor)
        task_setpriodin(aux, aux->prio_din-1);
    aux = aux->next; //anda para entrar no laço
    while (aux != first)
    {
        if (aux != menor) //se não for a tarefa escolhida (menor), ajusta prioridade dinamica
            task_setpriodin(aux, aux->prio_din-1);
        aux = aux->next;
    }

    task_setpriodin(menor, menor->prio); //retorna a prioridade dinamica para a estática (ler cap 2)
    return menor; //tarefa com maior prioridade)
}
#endif 
#ifndef PRIO
task_t* scheduler() //FCFS
{
	task_t *aux = ready;
	return aux;
}
#endif
/**Fim do #ifdef**/

//função do despachante
void dispatcher()
{
    task_t *next;
    while (ready > 0)
    {
        next = scheduler();
        if (next)
        {
            task_switch(next);
        }
    }
    task_exit(0);
}

// Inicializa o sistema operacional; deve ser chamada no inicio do main()
void pingpong_init()
{
    setvbuf(stdout, 0, _IONBF, 0); //padrão
	ticks = 0; //ticks do sistema é inicializado
    TaskMain.id = 0; //tarefa main tem id 0
	TaskMain.type = 'u'; //do tipo usuário
	TaskMain.ticks = 20; //entrega 20 ticks para a Main
    TaskEx = &TaskMain; //é a primeira a ser executada
    task_count = 1; //contador inicia em 1
	Dispatcher.type = 's'; //dispatcher é uma tarefa do sistema
    task_create(&Dispatcher, (void *)dispatcher, NULL); //cria tarefa dispatcher

	/********************************************/
	/*******Configuração do Signal e Time********/
	/********************************************/
	action.sa_handler = tratador_ticks; //função que será chamada a cada disparo
  	sigemptyset (&action.sa_mask);
  	action.sa_flags = 0 ;
  	if (sigaction (SIGALRM, &action, 0) < 0)
  	{
    	perror ("Erro em sigaction: ") ;
    	exit (1) ;
  	}
  	timer.it_value.tv_usec = 100;	// primeiro disparo, em micro-segundos
  	timer.it_interval.tv_usec = 1000;	// disparos subsequentes, em micro-segundos
  	if (setitimer (ITIMER_REAL, &timer, 0) < 0)
  	{
    	perror ("Erro em setitimer: ") ;
    	exit (1) ;
  	}
	/********************************************/
	/********************************************/
	/********************************************/

}

// Cria uma nova tarefa. Retorna um ID> 0 ou erro.
int task_create(task_t *task, void (*start_func)(void *), void *arg)
{
    if (task != NULL)
    {
        getcontext(&(task->task_context)); //inicializa o contexto
        /********************************************/
        char *stack;
        stack = malloc(STACKSIZE);
        if (stack)
        {
            task->task_context.uc_stack.ss_sp = stack ;
            task->task_context.uc_stack.ss_size = STACKSIZE;
            task->task_context.uc_stack.ss_flags = 0;
            task->task_context.uc_link = 0;
        }
        else
        {
            perror ("Erro na criação da pilha: ");
            exit (1);
        }
        /*********************************************/
        task->id = task_count; //atribui o id para a tarefa
        task_count++; //incrementa o contador
        makecontext(&(task->task_context), (void *)start_func, 1, arg); //adiciona a função no contexto
        if (task_count > 2) //evita que o Dispatcher entre na fila de prontas, veja que o if está após o incremento
		{
	        task_t *aux;
        	aux = task;
            queue_append((queue_t **) &ready,(queue_t *) aux); //adiciona tarefa na fila de prontas
			task->type = 'u';
		}
		task->inicio = systime();
		task->depend = NULL;
        return task->id; //retorna o id da tarefa
    }
    return -1; //retorna negativo em caso de erro
}

// Termina a tarefa corrente, indicando um valor de status encerramento
void task_exit(int exitCode)
{
	printf("Task %d exit: execution time %4d ms, processor time %4d ms, %d activations\n", task_id(), systime() - TaskEx->inicio, TaskEx->processor_use, TaskEx->activations); //imprime mensagem de saída
	TaskEx->exitCode = exitCode; //exitCode salvo na tarefa
	TaskEx->status = 't';
	if (task_id() != 1) //se a tarefa não for o dispatcher
	{
		queue_remove((queue_t **) &ready, (queue_t *) TaskEx); //remove tarefa da fila de prontas
		/********************************************************/
		if (queue_size ((queue_t *) suspend) > 0)
		{ 
			task_t *first = suspend; //primeira da fila de suspensas
			task_t *aux = suspend;
			task_t *depend = aux->depend;
			if (aux->depend == TaskEx)
			{
				queue_remove((queue_t **) &suspend, (queue_t *) aux);
				queue_append((queue_t **) &ready,(queue_t *) aux);
				aux->depend = NULL;
				aux->status = 'r';
			}
			aux = aux->next;
			while (aux != first)
			{
				if (aux->depend == TaskEx)
				{
					queue_remove((queue_t **) &suspend, (queue_t *) aux);
					queue_append((queue_t **) &ready,(queue_t *) aux);
					aux->depend = NULL;
					aux->status = 'r';
				}
				aux = aux->next;
			}
		}
		/******************************************************/
		task_switch(&Dispatcher); //devolve processador para o Despachante
	}
	/*else if(task_id() == 0)
	{
		queue_remove((queue_t **) &ready, (queue_t *) TaskEx); //remove tarefa da fila de prontas
		task_switch(&Dispatcher); //devolve processador para o Despachante
	}*/
	else //se for dispatcher
	{
		task_switch(&TaskMain); //devolve o processador para o Main
	}    
}

// alterna a execução para a tarefa indicada
int task_switch(task_t *task)
{
    if (task != NULL)
    {
        task_t *TaskAux = TaskEx;
        TaskEx = task;
		TaskEx->ticks = 20; //tarefa recebe 20 ticks
		TaskEx->activations++; //tarefa foi 'ativa', incrementa
        swapcontext(&TaskAux->task_context, &task->task_context);
        return 0;
    }
    return -1;
}

// retorna o identificador da tarefa corrente (main eh 0)
int task_id ()
{
    return TaskEx->id;
}
