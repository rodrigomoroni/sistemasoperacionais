// PingPongOS - PingPong Operating System
// Prof. Carlos A. Maziero, DAINF UTFPR
// Versão 1.0 -- Março de 2015
//
// Estruturas de dados internas do sistema operacional

#include<ucontext.h>

#ifndef __DATATYPES__
#define __DATATYPES__

#define STACKSIZE 32768 //tamanho da pilha

// Estrutura que define uma tarefa
typedef struct task_t
{
    struct task_t *prev; //ponteiro para a tarefa anterior
    struct task_t *next; //ponteiro para a próxima tarefa
    int id; //id da tarefa
    ucontext_t task_context; //contexto da tarefa
    char status; //status da tarefa, utilizei char por ser apenas 1 byte(s-suspend, r-ready...)
} task_t ;

// estrutura que define um semáforo
typedef struct
{
  // preencher quando necessário
} semaphore_t ;

// estrutura que define um mutex
typedef struct
{
  // preencher quando necessário
} mutex_t ;

// estrutura que define uma barreira
typedef struct
{
  // preencher quando necessário
} barrier_t ;

// estrutura que define uma fila de mensagens
typedef struct
{
  // preencher quando necessário
} mqueue_t ;

#endif
