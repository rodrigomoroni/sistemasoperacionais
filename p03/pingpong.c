#include <stdio.h>
#include <stdlib.h>
#include "pingpong.h"
#include "queue.h"

int task_count; //contador das tarefas (referencia para o id)

//TaskEx -> tarefa em execução, ready -> fila de prontos, suspend -> fila de suspensos
task_t TaskMain, Dispatcher, *TaskEx, *ready, *suspend;

// suspende uma tarefa, retirando-a de sua fila atual, adicionando-a à fila
// queue e mudando seu estado para "suspensa"; usa a tarefa atual se task==NULL
void task_suspend (task_t *task, task_t **queue)
{
    if (task == NULL) //se a tarefa for nula, pega a tarefa em execução
        task = TaskEx;
    task->status = 's'; //muda o status da tarefa para suspensa
    queue_append((queue_t **) queue,(queue_t *) task); //adiciona na fila
    if (queue != NULL) //se a fila não estiver vazia, remove da fila atual da tarefa
        queue_remove((queue_t **) &ready, (queue_t *) task);
}

// acorda uma tarefa, retirando-a de sua fila atual, adicionando-a à fila de
// tarefas prontas ("ready queue") e mudando seu estado para "pronta"
void task_resume (task_t *task)
{
    queue_remove((queue_t **) &suspend, (queue_t *) task); //remove da fila de suspensa
    task->status = 'r'; //muda o status da tarefa
    queue_append((queue_t **) &ready, (queue_t *) task); //adiociona na fila de prontos
}

//função que coloca a tarefa em execução no fim da fila de prontas
//e devolve o processador para o despachante
void task_yield()
{
    if (TaskEx != &TaskMain) //evita que o Main entre na fila
    {
		//move para o fim da fila
        queue_remove((queue_t **) &ready, (queue_t *) TaskEx);
        queue_append((queue_t **) &ready, (queue_t *) TaskEx);
    }
    task_switch(&Dispatcher); //devolve processador para o despachante
}

//escalonador FCFS, pega o primeiro na fila de prontas
task_t* scheduler()
{
    task_t *aux = ready;
    return aux;
}

//função da tarefa despachante que chama o escalonador
//enquanto houver tarefas na fila de prontas
void dispatcher()
{
    task_t *next; //vai receber a tarefa que receberá o processador
    while (ready > 0) //enquanto houver tarefas na fila
    {
        next = scheduler(); //escalonador retorna a tarefa selecionada
        if (next)
        {
            task_switch(next); //tarefa recebe o processador
        }
    }
    task_exit(0); //termina execução
}

// Inicializa o sistema operacional; deve ser chamada no inicio do main()
void pingpong_init()
{
    setvbuf(stdout, 0, _IONBF, 0); //padrão
    TaskMain.id = 0; //main tem id 0
    TaskEx = &TaskMain; //tarefa em execução é a main
    task_count = 1; //inicializa contador
    task_create(&Dispatcher, (void *)dispatcher, NULL); //cria tarefa despachante
}

// Cria uma nova tarefa. Retorna um ID> 0 ou erro.
int task_create(task_t *task, void (*start_func)(void *), void *arg)
{
    if (task != NULL)
    {
        getcontext(&(task->task_context)); //salva o contexto
		
		//padrão
        /********************************************/
        char *stack;
        stack = malloc(STACKSIZE);
        if (stack)
        {
            task->task_context.uc_stack.ss_sp = stack ;
            task->task_context.uc_stack.ss_size = STACKSIZE;
            task->task_context.uc_stack.ss_flags = 0;
            task->task_context.uc_link = 0;
        }
        else
        {
            perror ("Erro na criação da pilha: ");
            exit (1);
        }
        /*********************************************/

        task->id = task_count; //atribui o id para a tarefa
        task_count++; //incrementa o contador
        makecontext(&(task->task_context), (void *)start_func, 1, arg); //adiciona a função no contexto
        task_t *aux;
        aux = task;
        if (task_count > 2) //evita que o Dispatcher entre na fila de prontas, veja que o if está após o incremento
            queue_append((queue_t **) &ready,(queue_t *) aux); //adiciona tarefa na fila de prontas
        return task->id; //retorna o id da tarefa
    }
    return -1; //retorna negativo em caso de erro
}
// Termina a tarefa corrente, indicando um valor de status encerramento
void task_exit(int exitCode)
{
	if (task_id() > 1) //se a tarefa não for o dispatcher
	{
		queue_remove((queue_t **) &ready, (queue_t *) TaskEx); //remove tarefa da fila de prontas
    	task_switch(&Dispatcher); //devolve processador para o Despachante
	}
	else //se for dispatcher
	{
    	task_switch(&TaskMain); //devolve o processador para o Main
	}
}

// alterna a execução para a tarefa indicada
int task_switch(task_t *task)
{
    if (task != NULL)
    {
        task_t *TaskAux = TaskEx; //aux salva tarefa em execução
        TaskEx = task; //tarefa em execução agora é task
        swapcontext(&TaskAux->task_context, &task->task_context); //troca o contexto
        return 0;
    }
    return -1;
}

// retorna o identificador da tarefa corrente (main eh 0)
int task_id ()
{
    return TaskEx->id;
}
